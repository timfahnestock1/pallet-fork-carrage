Use V2.

Steel:
.25 .375 .5 plate cnc cut
8 feet 3x3x.188 square tubing
~29 inches 2x.5 flat bar

I would weld the fork rails onto the tubes first. they should be 1" above the face of the tube. 16" from the top of the top rail to the bottom of the bottom rail.
Then weld the 2 center gussets square on the bottom tube. Weld the top tube on making sure it is aligned side to side with the bottom one. The rest kinda just assembles itself


Version 1: https://youtu.be/kttVzM6MvyY
